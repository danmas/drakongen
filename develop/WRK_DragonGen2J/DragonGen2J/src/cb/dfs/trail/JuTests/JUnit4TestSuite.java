package cb.dfs.trail.JuTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses( { JuTrailRunner.class, JuTestTrailDb.class })
@RunWith(Suite.class)
public class JUnit4TestSuite {

}
